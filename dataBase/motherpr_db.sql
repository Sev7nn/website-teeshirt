-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
<<<<<<< HEAD
-- Hôte : localhost:3306
-- Généré le :  Dim 21 juil. 2019 à 21:53
-- Version du serveur :  5.6.44
-- Version de PHP :  7.2.7
=======
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 19 juil. 2019 à 16:57
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10
>>>>>>> parent of ece3fa7... prepare mise en prod

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `motherpr_db`
--
CREATE DATABASE IF NOT EXISTS `motherpr_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `motherpr_db`;

-- --------------------------------------------------------

--
-- Structure de la table `transaction`
--

<<<<<<< HEAD
CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `orderId` varchar(255) CHARACTER SET latin1 NOT NULL,
  `statut` varchar(255) CHARACTER SET latin1 NOT NULL,
  `montant` decimal(65,2) NOT NULL,
  `model` varchar(255) CHARACTER SET latin1 NOT NULL,
  `col` varchar(255) CHARACTER SET latin1 NOT NULL,
  `taille` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sexe` varchar(255) CHARACTER SET latin1 NOT NULL,
  `couleur` varchar(255) CHARACTER SET latin1 NOT NULL,
  `livraison` varchar(255) CHARACTER SET latin1 NOT NULL,
  `adresse` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ville_asso` varchar(255) CHARACTER SET latin1 NOT NULL,
  `filliere` varchar(255) CHARACTER SET latin1 NOT NULL,
  `prenom` varchar(255) COLLATE utf8_bin NOT NULL,
  `nom` varchar(255) CHARACTER SET latin1 NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `orderId`, `statut`, `montant`, `model`, `col`, `taille`, `sexe`, `couleur`, `livraison`, `adresse`, `ville_asso`, `filliere`, `prenom`, `nom`, `date`) VALUES
(1, '9671c3e3-a920-4812-833a-51f151f2e056', 'APPROUVED', '0.51', 'MP-T1-001', 'Rond', 'size_S', 'female_model', 'white_model', 'domicile', '1539 chemin du Carimail Mougins 06250', '', '', 'Jérémy', 'VILLET', '2019-07-21 21:51:28');

-- --------------------------------------------------------

--
-- Structure de la table `commande_test`
--

CREATE TABLE `commande_test` (
  `id` int(11) NOT NULL,
  `orderId` varchar(255) CHARACTER SET latin1 NOT NULL,
  `statut` varchar(255) CHARACTER SET latin1 NOT NULL,
  `montant` int(11) NOT NULL,
  `model` varchar(255) CHARACTER SET latin1 NOT NULL,
  `col` varchar(255) CHARACTER SET latin1 NOT NULL,
  `taille` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sexe` varchar(255) CHARACTER SET latin1 NOT NULL,
  `couleur` varchar(255) CHARACTER SET latin1 NOT NULL,
  `livraison` varchar(255) CHARACTER SET latin1 NOT NULL,
  `adresse` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ville_asso` varchar(255) CHARACTER SET latin1 NOT NULL,
  `filliere` varchar(255) CHARACTER SET latin1 NOT NULL,
  `prenom` varchar(255) COLLATE utf8_bin NOT NULL,
  `nom` varchar(255) CHARACTER SET latin1 NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `commande_valide`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `commande_valide` (
`id` int(11)
,`orderId` varchar(255)
,`statut` varchar(255)
,`montant` decimal(65,2)
,`model` varchar(255)
,`col` varchar(255)
,`taille` varchar(255)
,`sexe` varchar(255)
,`couleur` varchar(255)
,`livraison` varchar(255)
,`adresse` varchar(255)
,`ville_asso` varchar(255)
,`filliere` varchar(255)
,`prenom` varchar(255)
,`nom` varchar(255)
,`date` datetime
);

-- --------------------------------------------------------

--
-- Structure de la vue `commande_valide`
--
DROP TABLE IF EXISTS `commande_valide`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cpses_mouflulnis`@`localhost` SQL SECURITY DEFINER VIEW `commande_valide`  AS  select `commande`.`id` AS `id`,`commande`.`orderId` AS `orderId`,`commande`.`statut` AS `statut`,`commande`.`montant` AS `montant`,`commande`.`model` AS `model`,`commande`.`col` AS `col`,`commande`.`taille` AS `taille`,`commande`.`sexe` AS `sexe`,`commande`.`couleur` AS `couleur`,`commande`.`livraison` AS `livraison`,`commande`.`adresse` AS `adresse`,`commande`.`ville_asso` AS `ville_asso`,`commande`.`filliere` AS `filliere`,`commande`.`prenom` AS `prenom`,`commande`.`nom` AS `nom`,`commande`.`date` AS `date` from `commande` where (`commande`.`statut` like 'APPROUVED') ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commande_test`
--
ALTER TABLE `commande_test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `commande_test`
--
ALTER TABLE `commande_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
=======
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) NOT NULL,
  `statut` varchar(255) NOT NULL,
  `montant` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `col` varchar(255) NOT NULL,
  `taille` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `couleur` varchar(255) NOT NULL,
  `livraison` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `ville_asso` varchar(255) NOT NULL,
  `filliere` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
>>>>>>> parent of ece3fa7... prepare mise en prod
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
