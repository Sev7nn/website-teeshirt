
<?php

require_once('DbContext.php');


class TransactionDao
{
    public $dbContext;


    public function __construct()
    {
        $this->db = new DbContext();
    }

    public function createTransaction($orderId, $montant, $model, $col, $taille, $sexe, $couleur, $livraison, $adresse, $ville_asso, $filliere, $prenom, $nom)
    {
        $stmt = $this->db->context->prepare("INSERT INTO commande (`orderId`, `statut`,`montant`, `model`, `col`, `taille`, `sexe`, `couleur`, `livraison`, `adresse`, `ville_asso`, `filliere`, `prenom`, `nom`, `date`) 
        
                                            VALUES (?,'NOT APPROUVED',?,?,?,?,?,?,?,?,?,?,?,?,NOW())");
        $stmt->bindParam(1, $orderId);
        $stmt->bindParam(2, $montant);
        $stmt->bindParam(3, $model);
        $stmt->bindParam(4, $col);
        $stmt->bindParam(5, $taille);
        $stmt->bindParam(6, $sexe);
        $stmt->bindParam(7, $couleur);
        $stmt->bindParam(8, $livraison);
        $stmt->bindParam(9, $adresse);
        $stmt->bindParam(10, $ville_asso);
        $stmt->bindParam(11, $filliere);
        $stmt->bindParam(12, $prenom);
        $stmt->bindParam(13, $nom);
        $stmt->execute();
    }

  
    public function confirmTransaction($orderId, $montant)
    {

        $stmt = $this->db->context->prepare("UPDATE commande SET `statut` = 'APPROUVED'  
                                              WHERE orderId LIKE ? AND montant = ? ");
        $stmt->bindParam(1, $orderId);
        $stmt->bindParam(2, $montant);
        $stmt->execute();
    }
}

?>