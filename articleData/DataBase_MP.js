let mydata = {
  "teeshirt" :
    { "collection_1" :
      { "pharmacie" :
        [

          {
           "model": "text",
           "model_name":"",
           "titre_card": "",
           "description_card": "O",
           "keyword":"",
           "root": "",
           "price":""
          },
          {
           "model": "promo_newco",
           "model_name":"",
           "titre_card": "",
           "description_card": "",
           "keyword":"",
           "root": "images/ressource_site/collection2.png",
           "price":""
          },
          {
           "model": "MP-T1-001",
           "model_name":"Pharmacien Bar + 6",
           "titre_card": "Open Bar",
           "description_card": "On est pas bien là ?",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-002",
           "model_name":"Mémoire de l&#39eau N1",
           "titre_card": "La rêgle est simple :",
           "description_card": "5 volumes d&#39eau pour 1 de 51.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-003",
           "model_name":"J&#39peux pas j&#39ai mer",
           "titre_card": "Seulement deux dés",
           "description_card": "Et on sait tous comment ça va finir...",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-004",
           "model_name":"How to PLS",
           "titre_card": "Soyez courageux",
           "description_card": "Sauvez la vie de vos amies.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-005",
           "model_name":"J&#39peux pas j&#39ai internat",
           "titre_card": "L&#39internat",
           "description_card": "En résumé.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-006",
           "model_name":"Homéopathie fonctionne",
           "titre_card": "Attention à vos yeux...",
           "description_card": "On vous avez bien dit de ne pas lire...",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-007",
           "model_name":"Homéopathie dilution 1",
           "titre_card": "9 CH",
           "description_card": "ça serait bête d&#39être allergique au lactose.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-008",
           "model_name":"Homéopathie dilution 2",
           "titre_card": "15 CH",
           "description_card": "L&#39effet Placebo, quoi de mieux ?",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-009A",
           "model_name":"passion Naturopathie",
           "titre_card": "THC",
           "description_card": "Une passion envoutante",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-009B",
           "model_name":"Natural Painkiller",
           "titre_card": "Antalgique",
           "description_card": "Passe moi une feuille slim.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-010",
           "model_name":"Change de comptoir",
           "titre_card": "Patiente Pharmaceutique",
           "description_card": "Liste non exhaustive.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model":"photoDemo",
           "model_name":"description",
           "titre_card": "description",
           "description_card": "description",
           "keyword":"pharmacie",
           "root": "images/collection_t1/listeofficine",
           "price":""
          },
          {
           "model": "MP-T1-011",
           "model_name":"Apprentie Sorciere",
           "titre_card": "Hermione",
           "description_card": "Et au final ça pecho Ron Weasley",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model":"photoDemo",
           "model_name":"description",
           "titre_card": "description",
           "description_card": "description",
           "keyword":"pharmacie",
           "root": "images/collection_t1/sorciere_1",
           "price":""
          },
          {
           "model": "MP-T1-012",
           "model_name":"Apprentie Sorcier",
           "titre_card": "DANGER",
           "description_card": "A ne pas mettre entre toutes les mains.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-014",
           "model_name":"Poule suppo",
           "titre_card": "Info de derniere minute",
           "description_card": "La partie plate s&#39insère en premier.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-016",
           "model_name":"Business model",
           "titre_card": "Anyway",
           "description_card": "J&#39pas le time là, see u next time",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-018",
           "model_name":"Beer Pong",
           "titre_card": "Un jeu d&#39enfant",
           "description_card": "Même un manchot aveugle y arriverait.",
           "keyword":"pharmacie",
           "root":"images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-015",
           "model_name":"Hiroshima",
           "titre_card": "Il va falloir courir vite",
           "description_card": "Attention aux éclaboussures",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-019",
           "model_name":"Detendu",
           "titre_card": "Tou va bien s&#39passer",
           "description_card": "Le plus important c&#39est de pas rater la veine.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-023",
           "model_name":"Bacteria",
           "titre_card": "Pour une bacterie achetée...",
           "description_card": "Un Clostridium Difficile 027 offert",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model":"photoDemo",
           "model_name":"description",
           "titre_card": "description",
           "description_card": "description",
           "keyword":"pharmacie",
           "root": "images/collection_t1/bacterie",
           "price":""
          },
          {
           "model": "MP-T1-026A",
           "model_name":"Femme parfaite etoile",
           "titre_card": "Vous avez dit parfaite ?",
           "description_card": "Vous n&#39avez encore rien vu !",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model":"photoDemo",
           "model_name":"description",
           "titre_card": "description",
           "description_card": "description",
           "keyword":"pharmacie",
           "root": "images/collection_t1/femmeparfaite",
           "price":""
          },
          {
           "model": "MP-T1-024",
           "model_name":"Mémoire de l&#39eau N2",
           "titre_card": "Une seule règle :",
           "description_card": "Ne pas l&#39noyer.",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-025",
           "model_name":"Choinois",
           "titre_card": "Un petit homme a dit :",
           "description_card": "Tout les chemins mènent à Rome.",
           "keyword":"pharmacie",
           "root":"images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-017",
           "model_name":"Labo Pharmaceutique",
           "titre_card": "",
           "description_card": "",
           "keyword":"pharmacie",
           "root": "images/collection_t1/",
           "price":"16.00"
          },
          {
           "model": "MP-T1-013",
           "model_name":"Pharmacie d&#39officine",
           "titre_card": "Tee Shirt Officinal",
           "description_card": "L&#39officine ? Pour quoi faire ?",
           "keyword":"pharmacie",
           "root":"images/collection_t1/",
           "price":"16.00"
          }
        ],

        "evenement" :
        [
          {
           "model": "banniere",
           "model_name":"",
           "titre_card": "",
           "description_card": "Pharmacie",
           "keyword":"pharmacie",
           "root": "",
           "price":"16.00"
          },
          {
           "model": "MP-TE-001",
           "model_name":"",
           "titre_card": "",
           "description_card": "",
           "keyword":"pharmacie",
           "root": "images/collection_event/motherDay/",
           "price":"16.00"
          },
         {
          "model": "MP-TE-002",
          "model_name":"",
          "titre_card": "",
          "description_card": "",
          "keyword":"pharmacie",
          "root": "images/collection_event/motherDay/",
          "price":"16.00"
        },
        {
          "model": "MP-TE-003",
          "model_name":"",
          "titre_card": "",
          "description_card": "",
          "keyword":"pharmacie",
          "root": "images/collection_event/motherDay/",
          "price":"16.00"
        },
        {
         "model": "MP-TE-004",
         "model_name":"",
         "titre_card": "",
         "description_card": "",
         "keyword":"pharmacie",
         "root": "images/collection_event/motherDay/",
         "price":"16.00"
       },
       {
        "model": "MP-TE-005",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-006",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-007",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-008",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-009",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-010",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-010",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-011",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "MP-TE-012",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
       },
       {
        "model": "banniere",
        "model_name":"",
        "titre_card": "",
        "description_card": "Dentaire",
        "keyword":"dentaire",
        "root": "",
        "price":"16.00"
      },
       {
        "model": "MP-TE-013",
        "model_name":"",
        "titre_card": "",
        "description_card": "",
        "keyword":"pharmacie",
        "root": "images/collection_event/motherDay/",
        "price":"16.00"
      },

      {
       "model": "MP-TE-014",
       "model_name":"",
       "titre_card": "",
       "description_card": "",
       "keyword":"pharmacie",
       "root": "images/collection_event/motherDay/",
       "price":"16.00"
      },
      {
       "model": "MP-TE-015",
       "model_name":"",
       "titre_card": "",
       "description_card": "",
       "keyword":"pharmacie",
       "root": "images/collection_event/motherDay/",
       "price":"16.00"
     },
     {
      "model": "banniere",
      "model_name":"",
      "titre_card": "",
      "description_card": "Medecine",
      "keyword":"pharmacie",
      "root": "",
      "price":"16.00"
     },
      {
       "model": "MP-TE-016",
       "model_name":"",
       "titre_card": "",
       "description_card": "",
       "keyword":"pharmacie",
       "root": "images/collection_event/motherDay/",
       "price":"16.00"
      },
      {
       "model": "MP-TE-017",
       "model_name":"",
       "titre_card": "",
       "description_card": "",
       "keyword":"pharmacie",
       "root": "images/collection_event/motherDay/",
       "price":"16.00"
      },
      {
       "model": "MP-TE-018",
       "model_name":"",
       "titre_card": "",
       "description_card": "",
       "keyword":"pharmacie",
       "root": "images/collection_event/motherDay/",
       "price":"16.00"
      },
      {
       "model": "MP-TE-019",
       "model_name":"",
       "titre_card": "",
       "description_card": "Tout nos designs sont personnalisables sur demande",
       "keyword":"pharmacie",
       "root": "images/collection_event/motherDay/",
       "price":"16.00"
      }
    ],
    "colorful" :
    [
      {
       "model": "text",
       "model_name":"",
       "titre_card": "",
       "description_card": "",
       "keyword":"colorful",
       "root": "",
       "price":""
      },
      {
       "model": "banniere",
       "model_name":"",
       "titre_card": "",
       "description_card": "Colorful",
       "keyword":"colorful",
       "root": "",
       "price":""
     },
      {
       "model":"MP-T2-001",
       "model_name":"",
       "titre_card": "Paracetamol",
       "description_card": "",
       "keyword":"colorful",
       "root": "images/collection_t2/colorful_teeshirt/femme/",
       "price":"19.60"
      },
      {
       "model":"MP-T2-002",
       "model_name":"",
       "titre_card": "As Sweet As Homeopathy",
       "description_card": "",
       "keyword":"colorful",
       "root": "images/collection_t2/colorful_teeshirt/femme/",
       "price":"19.60"
      },
      {
       "model":"MP-T2-003",
       "model_name":"",
       "titre_card": "Under Psychotic",
       "description_card": "",
       "keyword":"colorful",
       "root": "images/collection_t2/colorful_teeshirt/femme/",
       "price":"19.60"
      },

      {
       "model":"MP-T2-005",
       "model_name":"",
       "titre_card": "Sildenafil",
       "description_card": "",
       "keyword":"colorful",
       "root": "images/collection_t2/colorful_teeshirt/homme/",
       "price":"19.60"
     },
     {
      "model":"MP-T2-006",
      "model_name":"",
      "titre_card": "Test ELISA",
      "description_card": "",
      "keyword":"colorful",
      "root": "images/collection_t2/colorful_teeshirt/femme/",
      "price":"19.60"
     },
     
     {
      "model":"MP-T2-008",
      "model_name":"",
      "titre_card": "Toxin",
      "description_card": "",
      "keyword":"colorful",
      "root": "images/collection_t2/colorful_teeshirt/femme/",
      "price":"19.60"
     },
     {
      "model":"MP-T2-009",
      "model_name":"",
      "titre_card": "Antidepressant",
      "description_card": "",
      "keyword":"colorful",
      "root": "images/collection_t2/colorful_teeshirt/homme/",
      "price":"19.60"
     },
     {
      "model":"MP-T2-010",
      "model_name":"",
      "titre_card": "Painkiller",
      "description_card": "",
      "keyword":"colorful",
      "root": "images/collection_t2/colorful_teeshirt/femme/",
      "price":"19.60"
     },
     {
      "model":"MP-T2-011",
      "model_name":"",
      "titre_card": "Natural Drug",
      "description_card": "",
      "keyword":"colorful",
      "root": "images/collection_t2/colorful_teeshirt/femme/",
      "price":"19.60"
     },
    ]
      }
    }

}
